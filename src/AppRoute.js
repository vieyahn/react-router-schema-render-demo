import React from "react";
import { Redirect, Route } from "react-router-dom";

const AppRoute = ({ component: Component, path, isPrivate, ...rest }) => {
    // FIXME: 预留的登录token，目前暂时设置全部为true
    const token = true;

    return (
        <Route
            path={path}
            render={props =>(
                isPrivate && !Boolean(token) ? (
                    <Redirect
                        to={{ pathname: "/home" }}
                    />
                ) : (
                    <Component {...props} />
                )
            )}
            {...rest}
        />
    )
}

export default AppRoute;
