import React from "react";
import {
    BrowserRouter as Router,
    Switch
} from 'react-router-dom'

import routes from './routes'
import NavBar from "./NavBar";
import AppRoute from "./AppRoute";

const App = () => {

    return (
        <Router>
            <NavBar isVisuable={true} />
            <Switch>
                {routes.map((route) => (
                    <AppRoute
                      key={route.path}
                      path={route.path}
                      component={route.component}
                      isPrivate={route.isPrivate}
                    />
              ))}
            </Switch>
        </Router>
    );
}

export default App;
