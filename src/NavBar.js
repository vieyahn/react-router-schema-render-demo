import React from 'react'
import { Link } from 'react-router-dom'

// FIXME: 我并不想NavBar显示在所有页面，增加一个bool参数控制，怎么弄
// 目前的 <NavBar isVisuable={false} />

const NavBar = ({ isVisuable }) => {

    if (!isVisuable) {
        return (
            <div msg="empty navbar">
            </div>
        );
    }

    return (
        <div>
            <ul>
                <li>
                    <Link to="/home">首页</Link>
                </li>
                <li>
                    <Link to="/contact">联系我们</Link>
                </li>
                <li>
                    <Link to="/about">关于我们</Link>
                </li>
                <li>
                    <Link to="/schema">关于schema</Link>
                </li>
                <li>
                    <Link to="/xxx">测试404</Link>
                </li>
            </ul>
        </div>
    );
}

export default NavBar;
