import React, {useState, useEffect} from 'react'
import axios from 'axios';
import { SchemaRender } from 'react-schema-render';

const Poeter = (props) => {
    const name = props.match.params.name;
    const [schemas, setSchemas] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            axios
            .get('/poeter.json')
            .then(res => {
                if(res.data) {
                    setSchemas(res.data);
                }
            })
            .catch((e) => {});
        }

        fetchData();

    }, []);

    const getSchema = (name) => {
        if (schemas.length === 0) {
            return [];
        }
        try {
            for(let i=0, leni = schemas.children.length; i < leni; i++) {
                if(name === schemas.children[i].header) {
                    return schemas.children[i];
                }
            }
        } catch(e) {
        };
        return schemas;
    };

    const schema = getSchema(name);

    return (
        <SchemaRender schema={schema}></SchemaRender>
    );

}

export default Poeter;
