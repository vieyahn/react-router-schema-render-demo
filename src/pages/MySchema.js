import React, {useState, useEffect} from 'react'
import { setComponents, SchemaRender } from 'react-schema-render';
import { Collapse } from 'antd';
import axios from 'axios';

const { Panel } = Collapse;
const components = {
    collapse: Collapse,
    panel: Panel,
};

setComponents(components);

const MySchema = () => {

    const [schemas, setSchemas] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            axios
            .get('/schema.json')
            .then(res => {
                if(res.data) {
                    setSchemas(res.data);
                }
            })
            .catch((e) => {});
        }

        fetchData();

    }, []);

    // console.log(schemas);
    const getSchema = (name) => {
        if (schemas[name]) {
            return schemas[name];
        }
        return schemas;
//        return {
//            "component": "div",
//            "children": Object.values(AllSchemas)
//        };
    };

    const schema = getSchema("this");

    return (
        <SchemaRender schema={schema}></SchemaRender>
    );
};

export default MySchema;
