import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = (props) => {
    return (
        <div>
            <h2>Not Found Page</h2>
            <p>Page not found!</p>
            <p>
                <Link to="/home">首页</Link>
            </p>
        </div>
    )
}

export default NotFound;
