import React from "react";
import { Route, Link } from "react-router-dom";

import Poeter from "./poets/Poeter";

const Contact = () => {
    return (
        <div>
            <h3>我是联系</h3>
            <p>
                这是一段联系的描述信息！
            </p>
            <h5>点击下方按钮，切换下面的描述信息</h5>
            <ol>
                <li>
                    <Link to="/contact/LiBai/">联系李白</Link>
                </li>
                <li>
                    <Link to="/contact/SuShi/">联系苏轼</Link>
                </li>
                <li>
                    <Link to="/contact/LuYou/">联系陆游</Link>
                </li>
                <li>
                    <Link to="/contact/LiQingzhao/">联系李清照</Link>
                </li>
                <li>
                    <Link to="/contact/Wuxxx/">联系吴慈仁</Link>
                </li>
            </ol>
            <Route path="/contact/:name/" component={Poeter} />
        </div>
    );
}

export default Contact;
