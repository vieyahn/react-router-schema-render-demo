import Home from "./pages/Home";
import Contact from "./pages/Contact";
import About from "./pages/About";
import MySchema from "./pages/MySchema";
import NotFound from './pages/NotFound';

const routes = [
    {
        path: "/home",
        component: Home,
        isPrivate: false
    },
    {
        path: "/contact",
        component: Contact,
        isPrivate: false
    },
    {
        path: "/about",
        component: About,
        isPrivate: false
    },
    {
        path: "/schema",
        component: MySchema,
        isPrivate: false
    },
    {
        path: "/*",
        component: NotFound,
        isPrivate: true
    }
]

/* FIXME: 根目录/访问的时候，会被匹配到NotFound */
export default routes;

