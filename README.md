## 效果
### react-router:
![1.jpg](./images/1.jpg)

### react-schema-render:
![2.jpg](./images/2.jpg)

## usage
npm install

npm run start

## refer
https://gitee.com/youling4438/react-router-dom-demo

https://gitee.com/dream2023/react-schema-render


## FIXME
目前3个FIXME，其实都是router相关问题：

/* FIXME: 根目录/访问的时候，会被匹配到NotFound */

// FIXME: 预留的登录token，目前暂时设置全部为true

// FIXME: 我并不想NavBar显示在所有页面，增加一个bool参数控制，怎么动态使用
